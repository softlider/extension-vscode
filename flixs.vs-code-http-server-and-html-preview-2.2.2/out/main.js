"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const path = require("path");
const fs_1 = require("fs");
const logger_1 = require("./modules/logger");
const http_server_1 = require("./modules/http_server");
class Main {
    static main(context) {
        this.logger.log("The 'HTTP Server / HTML Preview' extension is now active!");
        this.registerCommands(context);
        this.setupRefreshHandler();
    }
    static registerCommands(context) {
        const createServer = vscode.commands.registerCommand("shs.createServer", () => {
            const file = vscode.workspace.getConfiguration("shs").get("mainFile");
            if (!fs_1.existsSync(path.join(vscode.workspace.rootPath, file))) {
                this.logger.logWarn(`Cannot find main file '${file}', please make sure that you are in the correct directory!`, true);
                return;
            }
            this.httpServer.setup(file);
        });
        const createServerWithSingleFile = vscode.commands.registerCommand("shs.createServerWithSingleFile", () => {
            let file = vscode.window.activeTextEditor.document.fileName;
            if (file.startsWith("Untitled")) {
                this.logger.logWarn("The current file has to be saved on your computer in order to be launched with a HTTP server!", true);
                return;
            }
            this.httpServer.setup(file.substring(vscode.workspace.rootPath.length));
        });
        context.subscriptions.push(createServerWithSingleFile, createServer);
    }
    static setupRefreshHandler() {
        vscode.workspace.onDidSaveTextDocument(() => this.httpServer.refresh());
    }
}
Main.logger = new logger_1.Logger("Main");
Main.httpServer = new http_server_1.HTTPServer();
exports.Main = Main;
//# sourceMappingURL=main.js.map