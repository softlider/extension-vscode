"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const vscode = require("vscode");
const http = require("http");
const opn = require("opn");
const isRoot = require("is-root");
const webview_1 = require("../modules/webview");
const websocket_1 = require("./websocket");
const logger_1 = require("./logger");
const refresher_1 = require("./refresher");
const app = express();
class HTTPServer {
    constructor() {
        this.logger = new logger_1.Logger("HTTP-Server");
        const path = vscode.workspace.rootPath;
        app.use("/", (request, response) => {
            let url = decodeURI(request.originalUrl);
            if (url == "/") {
                response.send(refresher_1.Refresher.getRefreshHandler(this.mainFile));
                return;
            }
            response.sendFile(path + url);
        });
    }
    setup(mainFile) {
        this.mainFile = mainFile;
        this.setupServer();
        if (this.httpServer)
            this.websocket = new websocket_1.WebSocket(this.httpServer);
    }
    setupServer() {
        const config = vscode.workspace.getConfiguration("shs");
        const port = config.get("serverPort");
        const host = config.get("serverHost");
        if (process.platform !== "win32" && port < 1024 && !isRoot()) {
            this.logger.logWarn("You need sudo permissions to run ports below '1024'", true);
            return;
        }
        if (this.httpServer)
            this.httpServer.close();
        this.httpServer = http.createServer(app);
        this.httpServer.listen(port, host, () => this.onHTTPServerListening(`${host}:${port}`));
    }
    refresh() {
        this.websocket.notifyRefresh();
    }
    onHTTPServerListening(address) {
        address = address === "0.0.0.0" ? "127.0.0.1" : address;
        this.logger.logSucess(`Server listening on '${address}'`);
        vscode.window.showInformationMessage(`Simple HTTP Server running on '${address}'`, "Open in Browser", "Open in Visual Studio")
            .then(option => {
            if (!option)
                return;
            if (option == "Open in Browser")
                opn("http://" + address);
            else if (option == "Open in Visual Studio")
                webview_1.Webview.showWebsite(address);
        });
    }
}
exports.HTTPServer = HTTPServer;
//# sourceMappingURL=http_server.js.map