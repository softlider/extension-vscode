"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ws = require("ws");
const logger_1 = require("./logger");
class WebSocket {
    constructor(server) {
        this.logger = new logger_1.Logger("WebSocket");
        this.sockets = [];
        this.websocket = new ws.Server({ server });
        this.setupConnectionHandler();
        this.logger.log("Created websocket, listing");
    }
    setupConnectionHandler() {
        this.websocket.on("connection", socket => {
            this.logger.log("Socket connected");
            this.sockets.push(socket);
            socket.onclose = () => {
                this.logger.log("Socket disconnected");
                this.sockets.splice(this.sockets.indexOf(socket), 1);
            };
        });
    }
    notifyRefresh() {
        this.logger.log("Notifying clients about refresh");
        this.sockets.forEach(socket => {
            socket.send("refresh", error => {
                if (!error)
                    return;
                this.logger.logError(error);
            });
        });
    }
    close() {
        this.logger.log("Closing websocket");
        this.websocket.close();
    }
}
exports.WebSocket = WebSocket;
//# sourceMappingURL=websocket.js.map