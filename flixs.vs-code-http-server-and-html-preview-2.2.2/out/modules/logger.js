"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const notifyer_1 = require("./notifyer");
class Logger {
    constructor(name) {
        this.ExtensionName = "Simple HTTP Server";
        this.prefix = `[${this.ExtensionName} - ${name}]`;
    }
    internalLog(message, type, notifyUser) {
        let text = !!type
            ? `${this.prefix} [${type}] ${message}`
            : `${this.prefix} ${message}`;
        console.log(text);
        if (notifyUser)
            notifyer_1.Notifyer.addUserNotification(text, type);
    }
    log(message, notifyUser) {
        this.internalLog(message, null, notifyUser);
    }
    logError(error, notifyUser) {
        const text = `${this.prefix} [ERROR] ${(error instanceof Error) ? error.message : error}`;
        console.error(text);
        if (notifyUser)
            notifyer_1.Notifyer.addUserNotification(text, "ERROR");
    }
    logSucess(message, notifyUser) {
        this.internalLog(message, "SUCCESS", notifyUser);
    }
    logInfo(message, notifyUser) {
        this.internalLog(message, "INFO", notifyUser);
    }
    logDebug(message, notifyUser) {
        this.internalLog(message, "DEBUG", notifyUser);
    }
    logWarn(message, notifyUser) {
        this.internalLog(message, "WARN", notifyUser);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map