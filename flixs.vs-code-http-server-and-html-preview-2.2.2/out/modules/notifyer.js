"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
class Notifyer {
    static addUserNotification(text, type) {
        const window = vscode.window;
        switch (type) {
            case "WARN":
                window.showWarningMessage(text);
                break;
            case "ERROR":
                window.showErrorMessage(text);
                break;
            default:
                window.showInformationMessage(text);
        }
    }
}
exports.Notifyer = Notifyer;
//# sourceMappingURL=notifyer.js.map