"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
function activate(context) { main_1.Main.main(context); }
exports.activate = activate;
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map